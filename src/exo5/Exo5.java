package exo5;

public class Exo5 {

	public static void main(String[] args) {
//		TP2
//		Ecrire une fonction nombrePartiesGagnees 
//		retournant le nombre de parties gagn�es d�un jouer, 
//		la fonction re�oit en param�tre deux tableaux : 
//			- Un tableau scoreJoueur qui contient les nombres de points obtenus par le joueur � chaque partie jou�e. 
//			- Un tableau scoreAdversaire qui contient les nombres de points obtenus par le joueur adverse aux m�me parties. 
//		2.Ecrire une fonction Gagnant retournant : 
//			- True si le nombre de partie gagn�es est sup�rieur au nombre de parties jou�es 
//			divis� par deux (plus de partie gagn�es que perdues). 
//			False si non 
//			Cette fonction re�oit les m�mes param�tres que la fonction pr�c�dente
		
		int[] tableauJoueur = {512,420,1020,1440,800};
		int[] tableauAdvers = {800,840,240,380,820};

		System.out.println("Le joueur a gagn� " + combienGagnees(tableauJoueur, tableauAdvers) + " parties");
		
		if(joueurGagne(tableauJoueur, tableauAdvers)) {
			System.out.println("Le joueur a gagn� ");
		} else {
			System.out.println("Le joueur a perdu ");
		}
		
	}
	
	static int combienGagnees(int[] joueur, int[] adversaire) {
		int gagnees = 0;
		
		for(int i = 0; i < joueur.length; i++) {
			if (joueur[i] >= adversaire[i]) {
				gagnees ++;
			}
		}
		return(gagnees);
	}

	static boolean joueurGagne(int[] joueur, int[] adversaire) {
		if (combienGagnees(joueur, adversaire) > (joueur.length/2)) {
			return(true);
		} else {
			return(false);
		}
	}
}
