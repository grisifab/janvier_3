package exo3;

public class Exo3 {

	public static void main(String[] args) {
		// �crire les fonctions suivantes.  
		// Une fonction qui renvoie la plus grande de deux valeurs de type int.  
		// Une fonction qui r�p�te un m�me mot un certain nombre de fois au choix.  
		// Une fonction, construite � partir de la fonction Math.random, qui tire au sort un nombre entier entre deux bornes donn�es en arguments. 
		// Une fonction qui teste si un tableau contient une valeur sp�cifique 
		
		System.out.println(definirPlusGrand(12, 24));
		
		perroquet("coco ", 12);
		
		System.out.println(hasardBornes(-1200, 24));
		
		int[] leTab = {12,24,36,48};
		
		System.out.println(presenceDansTableau(leTab, 12));
		
		
	}
	
	static int definirPlusGrand(int entier1, int entier2) 
	{
		return(Math.max(entier1, entier2));
	}
	
	static void perroquet(String leMot, int leNombre) 
	{
		for(int i = 0; i < leNombre; i++) {
			System.out.println(leMot);
		}
	}
	
	static int hasardBornes(int val1, int val2) 
	{
		int result;
		result = (int)(Math.random() * Math.abs(val1 - val2));
		result += Math.min(val1, val2);
		return(result);
	}
	
	static boolean presenceDansTableau(int[] tableau, int valeur) 
	{
		boolean result = false;
		for(int i = 0; i<tableau.length; i++) {
			if(tableau[i] == valeur) {
				result = true;
			}
		}
		return(result);
	}
}
