package exo2;

public class Exo2 {

	public static void main(String[] args) {
		// �crire l�en-t�te des fonctions suivantes : 
		// Une fonction qui calcule la vitesse moyenne d�une voiture connaissant 
		// son temps de parcours et la distance parcourue
		// Une fonction qui indique s�il est possible de construire un triangle 
		// avec trois segments de mesures donn�es. 
		// Une fonction qui calcule le plus grand diviseur commun (PGCD) de deux nombres entiers.  
		// Une fonction qui trace � l��cran un segment entre deux points.  
		// Une fonction qui �crit � l��cran les initiales d�une personne dont on donne le nom complet.
		
		System.out.println("La vitesse moyenne est de " + calculMoyVitesse(2, 300) + " Km/h");
		
		if (possibleTriangle(5, 3, 3)) {
			System.out.println("Triangle possible");
		} else {
			System.out.println("Triangle impossible");
		}
		
		System.out.println("Le plus grand diviseur commun est : " + calculPGCD(1900,300));
		
		initialesNom("Guillaume de la Cour");
	}
	
	static float calculMoyVitesse(float temps, float distance) {
		if (temps > 0) {
			return(distance/temps);
		} else {
			return(0);
		}
	}
	
	static boolean possibleTriangle(float l1, float l2, float l3) {
		
		float sommeLongeur = l1 + l2 + l3;
		float maxLongeur = Math.max(l1, Math.max(l2, l3));

		if (maxLongeur < sommeLongeur / 2) {
			return (true);
		} else {
			return (false);
		}
	}

	static int calculPGCD(int val1, int val2) {
		
		for(int i = Math.min(val1, val2); i >= 1; i--) {
			if((val1 % i == 0)&&(val2 % i == 0)) {
				return(i);
			}
		}
		return(1);
	}
	
	static void traceSegment(float p1X, float p1Y, float p2X, float p2Y) {}
	
	static void initialesNom(String nomDuGars) {
		int indice;
		
		System.out.println("Les initiales de " + nomDuGars + " sont :" );
		System.out.print(nomDuGars.charAt(0));
		
		indice = nomDuGars.indexOf(' ');
		
		while (indice != -1) {
			System.out.print(nomDuGars.charAt(indice+1));
			nomDuGars = nomDuGars.substring(0, indice) + '_' + nomDuGars.substring(indice+1);
			indice = nomDuGars.indexOf(' ');
		}
	}
}
